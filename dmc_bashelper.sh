#!/bin/bash
# SPDX-FileCopyrightText: 2022 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# chmod 664 /usr/local/bin/dmc_bashelper.sh && chown root:root /usr/local/bin/dmc_bashelper.sh
# 2022-03-08

readonly HL='\033[1;32m' # Bold Green
readonly WL='\033[1;33m' # Bold Yellow
readonly NL='\033[0m'    # Text Reset

f_error_trap() {

  local s=$?                   # error status
  local line=${BASH_LINENO[0]} # LINENO
  local linecallfunc=$1        # line where func was called
  local funcstack=${2:-}       # funcname
  #if [[ "$s" != "0" ]]; then
  echo "$(date +%F_%H%M%S) - $0: ERROR ($s) -> line $line: $BASH_COMMAND"
  if [ "$funcstack" != "" ]; then
    echo "   ... at function ${funcstack[0]}() called at line $linecallfunc" #${FUNCNAME[1]}
  fi
  #fi # condicial si hacemos trap con EXIT
  exit $s
}
trap 'f_error_trap $BASH_LINENO ${FUNCNAME:-}' ERR

exitWithErrMsg() {
  printf '%s\n' "$1" >&2
  exit 1
}

needs_bash() {
  # check Bash version (associative arrays need Bash in version 4.0+)
  if ((BASH_VERSINFO[0] < 4)); then
    exitWithErrMsg "Script needs Bash in version 4.0 or newer. Aborting."
  fi
}

get_script_info() {
  # Script name
  readonly SCRIPTNAME=$(basename -- $0)
  # Absolute path to this script
  readonly SCRIPT=$(readlink -f "$0")
  # Absolute path this script is in
  readonly SCRIPTPATH=$(dirname "$SCRIPT")
  # https://stackoverflow.com/questions/35006457/choosing-between-0-and-bash-source
  # https://stackoverflow.com/questions/14835193/how-do-i-get-the-script-name-being-executed-in-bash
}

check_root() {
  if ((${EUID:-0} || "$(id -u)")); then
    echo "This script must be run as root" 1>&2
    exit 1
  fi
}

debug_time() {
  date +%F_%H%M
}

f_pinta() {
  case $1 in
  h | H | verde | highlight)
    echo -e "\n${HL}$(debug_time) : $2 ${NL}"
    ;;
  w | W | amarillo | warning)
    echo -e "\n${WL}$(debug_time) : $2 ${NL}"
    ;;
  *)
    echo -e "\n$(debug_time) : $2"
    ;;
  esac
}

f_borraf() {
  local faborrar=$1
  rm -f "$faborrar" && f_pinta W "Borrado $faborrar"
}

f_borrar_old() {
  local RUTA=$1
  local DIAS=$2
  find "$RUTA" -mtime "$DIAS" -delete
  #find /media/usb0/motion/*/* -type f -mtime +7 -delete
}

f_asegura_parametros() {
  # This functions IS case-sensitive, if option not found it will append it at the bottom
  [ -z "$1" ] || FILESEC=$1
  [ -z "$2" ] || PARAMS=$2
  PARAV="${3:-}" # If there is no value in $3 it will just uncomment the param ($2)
  [ -z "$PARAMS" ] && {
    echo -e "${WL} Option required but it's not defined. Aborting. ${NL}" >&2
    exit 23
  }
  echo -e "${HL} Configurando $FILESEC ${NL}"
  for PARAM in $PARAMS; do
    if grep -q "^#*${PARAM}" "$FILESEC"; then
      echo -e "${HL}  Existe: $PARAM Asegurando valor... ${NL}"
      grep -qx "^${PARAM}${PARAV}" "$FILESEC" || sed -i "s/^#*${PARAM}.*/#& # DMC/ ; /^#*${PARAM}/a ${PARAM}${PARAV}" "$FILESEC"
    else
      echo -e "${WL}  Añadiendo $PARAM en $FILESEC ${NL}"
      if grep -q "^#DMC no definidos" "$FILESEC"; then
        printf "\n%s%s" "${PARAM}" "${PARAV}" >>"$FILESEC"
      else
        printf "\n#DMC no definidos:\n%s%s" "${PARAM}" "${PARAV}" >>"$FILESEC"
      fi
    fi
  done
}

f_establece_parametros() {
  # This functions IS case-sensitive, if option not found it will append it at the bottom
  [ -z "$1" ] || FILESEC=$1
  [ -z "$2" ] || PARAMS=$2
  PARAV="${3:-}" # If there is no value in $3 it will just uncomment the param ($2)
  [ -z "$PARAMS" ] && {
    echo -e "${WL} Option required but it's not defined. Aborting. ${NL}" >&2
    exit 23
  }
  echo -e "${HL} Configurando $FILESEC ${NL}"
  for PARAM in $PARAMS; do
    if grep -q "^#*${PARAM}" "$FILESEC"; then
      echo -e "${HL}  Existe: $PARAM Asegurando valor... ${NL}"
      grep -qx "^${PARAM}${PARAV}" "$FILESEC" || sed -i "s!^#*${PARAM}.*!${PARAM}${PARAV}!" "$FILESEC"
    else
      echo -e "${WL}  Añadiendo $PARAM en $FILESEC ${NL}"
      printf "\n%s%s" "${PARAM}" "${PARAV}" >>"$FILESEC"
    fi
  done
}

f_existe_comando() {
  #command -v foo >/dev/null 2>&1 || { echo >&2 "I require foo but it's not installed.  Aborting."; exit 1; }
  if command -v "$1" >/dev/null 2>&1; then
    declare "DMCV_${1//-/_}=0"
    return 0
  else
    declare "DMCV_${1//-/_}=1"
    return 11
  fi
}

f_fichero_usable() {
  # If file is not empty and readable
  [[ -s $1 && -r $1 ]]
}

f_get_config() {
  # Si hay un fichero de configuracion lo leemos que sobreescriba/setee las Variables
  # https://stackoverflow.com/questions/16571739/parsing-variables-from-config-file-in-bash
  LOCALCONF="${1-/usr/local/etc/config_$0}" # set the actual path name of your (DOS or Unix) config file
  if [[ -s $LOCALCONF && -r $LOCALCONF ]]; then
    #tr -d '\r' < $LOCALCONF > $LOCALCONF.unix  # deletes DOS carriage return
    local lhs
    local rhs
    while IFS='= ' read -r lhs rhs; do
      # ! $lhs =~ ^\ *# skips single line comments and -n $lhs skips empty lines
      if [[ ! $lhs =~ ^\ *# && -n $lhs ]]; then
        rhs="${rhs%%\#*}" # Del in line right comments
        # rhs="${rhs%%*( )}"   # Del trailing spaces, needs shopt -s extglob
        rhs="${rhs%\"*}"         # Del opening string quotes
        rhs="${rhs#\"*}"         # Del closing string quotes
        declare -g "$lhs"="$rhs" # "-g" solo disponible en bash => 4.2
      fi
    done <"$LOCALCONF"
  else
    f_pinta W 'NO hay fichero de configuracion local:'
    exitWithErrMsg "$LOCALCONF"
  fi
}

f_check_ip() {
  local IPACTUAL
  local IPFILE="$HOME/ipactual.txt"
  IPACTUAL=$(curl -s ifconfig.me)
  if ! grep -qc "$IPACTUAL" "$IPFILE"; then
    echo "$IPACTUAL" | tee "$IPFILE"
  fi
}

f_check_file_sec() {
  find /etc /home /var -type f -perm /o=r -execdir grep -iq "pass.*[=:].+" {} \; -ls 2>/dev/null
}

f_distribucion() {
  F_osrelease=/etc/os-release
  if [ -s $F_osrelease ]; then
    OS_ID=$(grep "^ID=" $F_osrelease | awk -F= '{print $2}' | tr -d '"')
    if [ -z "${OS_ID}" ]; then
      OS_ID='empty'
      echo "Check $F_osrelease"
    fi
  else
    OS_ID='no_os-file'
  fi
  echo "$OS_ID"
}

f_entornoescritorio() {
  for de in gnome kde xfce lxde; do
    echo -n " Procesos de $de : "
    if pgrep -c $de; then
      ENTORNO=$de
    fi
  done
  echo $ENTORNO
}

# f_show_variables () {
#   ( set -o posix ; set )
# }

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
# https://disconnected.systems/blog/another-bash-strict-mode/
# https://fvue.nl/wiki/Bash:_Error_handling

# https://devhints.io/bash

# command -v is POSIX and well defined
# https://stackoverflow.com/questions/592620/how-to-check-if-a-program-exists-from-a-bash-script
# https://unix.stackexchange.com/questions/85249/why-not-use-which-what-to-use-then
# https://stackoverflow.com/questions/2953081/how-can-i-write-a-heredoc-to-a-file-in-bash-script
# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_07_04
