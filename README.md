<!--
SPDX-FileCopyrightText: 2021 David Marzal

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Vigilante Casero

Este proyecto contiene las instrucciones y un script para montar un sistema de vigilancia en casa con una Raspberry Pi

[[_TOC_]]

## Funcionamiento

A grandes rasgos lo que este proyecto hace es utilizar la capacidad de [motion](https://motion-project.github.io/) de generar eventos al detectar movimientos para mandar una foto y un mensaje a un canal de Telegram, alertandonos de cuando hay movimiento en nuestra casa (o localización donde lo tengamos montado).
* La configuración nos permite definir las IPs de nuestros dispositivos moviles (se recomienda configurar IP fija o por MAC) para detectar si estamos en casa o no.
* En función de si estamos en casa o no el programa actua de la siguiente manera:
    * **Estamos en casa** (o alguno de nuestros dispositvos lo esta) -> Solo nos mandara un mensaje de aviso _silencioso_, sin fotografía. Ademas los mensajes tendran un tiempo mínimo entre ellos definido en el fichero de configuración (por defecto 30 minutos), para que no tengamos mensajes constantes si estamos moviendonos por casa.
        * Los videos generados se guardan en una subcarpeta con la IP del primer dispositivo detectado y se borraran los que tengán más de 7 días.
    * **No estamos** en casa/no se detectan dispositvos -> En este caso se nos enviará:
        * Un primer mensaje de PREAVISO para saber que ha detectado algo. (Por si se cortara la conexión o pasara algo).
        * Y despues una foto una vez que finalice el video momento en el que motion detecta movimiento. Borrandose el mensaje del PREAVISO
        * Los videos se guardan en el raiz de la ruta especificada en la configuración para el almacenaje y se borraran los que tengán más de 1 mes.
* Mediante la configuración opcional de una VPN como Wireguard y la apertura del puerto correspondiente en el _router_ se puede tener acceso a la webcam en directo. Pero está configuración execede el alcance de esta documentación.
    * El programa si se encarga de enviarnos un mensaje si detecta que nuestro router ha cambiado de IP pública para que sea posible acceder a la VPN sin DNS.
* Con pequeños cambios este programa podría funcionar en cualquier distribución con bash, pero ahora mismo está configurada y probada para RPi 2 y 3.

## Instalación

### Telegram
Es necesario:
* Crear bot con @botfather
* Añadirlo a un canal (mejor si es privado)
* Apuntar el `TOKEN` del bot y el `ID` del canal

### Raspberry Pi

Es preferible que ya tengamos [motion](https://motion-project.github.io/4.1.1/motion_guide.html) funcionando en raspbian, pero si no, el comando `install script` lo intentará hacer por nosotros.

* Clonar el repo con `git clone https://gitlab.com/Marzal/vigilante-casero.git`
    * El programa debería funcionar desde esa misma carpeta pero tiene la opción de instalarse en `/usr/local/{etc,bin}`
* Rellenar los datos de configuración en `vigilantecasero.conf`
* Configurar `motion` para que ejecute el script cada vez que guarde una imagen con una de estas dos opciones en `/etc/motion/motion.conf`:
    * Editando las lineas que contienen al menos los siguientes parametros: `on_event_start`, `on_picture_save` y `target_dir`
        * `on_event_start` es opcional. Solo si queremos un preaviso por si se corta la corriente o falla algo antes de enviar la imagen.
    * El comando `vigilantecasero.sh install script` añadira los comandos necesarios apuntando a `/usr/local/bin/vigilantecasero.sh`, ademas de moficiar varias opciones.
* Tener el punto de montaje listo donde montaremos RUTAVIDEOS en `/etc/fstab`
    * Ejemplo para sistemas no ext4:
    * `UUID=9B71-C4E8 /media/usb0 exfat noatime,lazytime,rw,gid=video,fmask=0002,dmask=0002,nofail,noauto,x-systemd.automount`

## Configuración

Poder hacer funcionar `motion` y el bot de Telegram son requisitos de este proyecto.

### vigilantecasero.conf

* **TOKEN** [Obligatorio] -> Token que nos ha dado @botfather.
* **IDR** [Obligatorio] -> ID del canal donde recibiremos las notifiaciones. (Si empeiza por **-100** esa parte no es necesaria).
    * Para obtener el id del canal podeis escribir en el algún mensaje, y luego le dais a "Copiar el enlace del post". Os dara una URL del tipo `https://t.me/c/`**IDR**`/1123`
* **RUTAVIDEOS** [Obligatorio]  -> Ruta completa de la carpeta donde queremos que se guarden los videos de motion (se recomienda un pincho USB).
* **IPs2WATCH** [Opcional] -> Listado de IPs locales separadas por espacios en blanco, de las IPs que queremos que se usen para detectar nuestra presencia en casa. Por defecto `ninguna`
* **TIEMPOMIN** [Opcional] -> Tiempo (en minutos`240`) a esperar para generar una nueva notificación si se detecta movimiento estando nostros en casa.
* **TIEMPORET** [Opcional] -> Periodo de gracia en minutos`5` para evaluar si estamos en casa. Si ya nos ha detectado antes de este tiempo no reevalua la presencia.
* **MODO**  [Opcional] -> `normal` o silencioso (sin de alertas en las que no nos detecta en casa, pero si estabamos hace al menos el tiempo establecido en  [TIEMPORET]).
* **DEPURA** [Opcional] -> Si queremos que el programa muestre más información en su log. Por defecto en `false`

Ejemplo:
```ini
TOKEN=123456789:ABCDefghijkl12rfdhj233jfdkfk
IDR=0123456789
RUTAVIDEOS=/media/usb0/motion
IPs2WATCH="192.168.1.15 192.168.1.14 192.168.1.12"
TIEMPOMIN=240
TIEMPORET=5
DIASPARAENCASA=3
DIASPARANOCASA=14
MODO=normal
DEPURA=false
```

### motion ###

* Si hemos lanzando el comando `vigilantecasero.sh install script` en princpio todo está listo para configurar pero conviene comprobar opciones como:
    * **/etc/motion/motion.conf**
        * `width`
        * `height`
        * Los que están en la función `install_motion` dentro del script
* En caso de querer configurarlo a mano hay que modificar obligatoriamente estas opciones:
    * **/etc/motion/motion.conf**
        * `target_dir $RUTAVIDEOS`
        * `on_event_start RUTA/vigilantecasero.sh preavisa %v >> /tmp/vigilantecasero.log 2>&1`
        * `on_picture_save RUTA/vigilantecasero.sh avisa %f >> /tmp/vigilantecasero.log 2>&1`
    * **/etc/default/motion**
        * Hay que activar el demonio poniendo a `yes` la opción `start_motion_daemon`
    * Se recomiendo activar el servicio para que se lance tras un reinicio: `systemctl enable motion --now`

## Recomendaciones

### Usar tmpfs para el /tmp
Añadiendo la siguiente linea a `/etc/fstab` conseguimos descargar el uso de la microSD y acelerar varios procesos de la placa
```bash
tmpfs /tmp tmpfs defaults,noatime,nosuid 0 0
```

## Hardware
Este proyecto ha sido probado con los siguientes dispositivos.

### Placa SBC

* RPi 2 : (Segunda mano)
* RPi 3A+ : 27 / 32 € (Nueva)
    * DietPi_RPi-ARMv8-Bullseye

### Webcam

* (USB) Sony Play Station Eye Camera for PS3 : 3€ de segunda mano
* (CSI) Longruner LC26-US [con LEDs+infrarrojos] : 29€

### Memoría

* MicroSD de al menos 4GB. Recomendable 8GB.
    * DietPi_RPi-ARMv8-Bullseye ocupa con todo ya instalado 3GB
* Pincho USB de 8GB. Cuanto más grande más tiempo se pueden retener las grabaciones.
