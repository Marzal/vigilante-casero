#!/bin/bash

# SPDX-FileCopyrightText: 2022 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# chmod 570 /usr/local/bin/vigilantecasero.sh && chown motion:root /usr/local/bin/vigilantecasero.sh
# 2022-05-20
# Script que puede ser lanzado por un evento de motion cada vez que detecte movimiento

set -eEuo pipefail

# Get helper functions
readonly bindir=/usr/local/bin
readonly funciones=dmc_bashelper.sh
readonly etcdir=/usr/local/etc
readonly configuracion=vigilantecasero.conf

f_get_ruta() {
  local ruta=$1
  local fichero=$2
  if [[ -s $fichero && -r $fichero ]]; then
    rutafichero="./$fichero"
    # the helper script takes precedence in the same folder
  elif [[ -s $ruta/$fichero && -r $ruta/$fichero ]]; then
    rutafichero="$ruta/$fichero"
  else
    echo "Este script necesita $fichero"
    exit 1
  fi
}
f_get_ruta $bindir "$funciones"
# shellcheck source=dmc_bashelper.sh
source "$rutafichero" || (
  echo "Fallo al cargar $funciones"
  exit 2
)

# Modificamos la función para que lo mande por Telegram tambien
f_error_trap() {
  local s=$?                   # error status
  local line=${BASH_LINENO[0]} # LINENO
  local linecallfunc=$1        # line where func was called
  local funcstack=${2:-}       # funcname
  #if [[ "$s" != "0" ]]; then
  echo "$(date +%F_%H%M%S) - $0: ERROR ($s) -> line $line: $BASH_COMMAND"
  f_mensaje_ss "$0: ERROR ($s) -> line $line: $BASH_COMMAND" # addition
  if [ "$funcstack" != "" ]; then
    echo "   ... at function ${funcstack[0]}() called at line $linecallfunc" #${FUNCNAME[1]}
  fi
  #fi # condicial si hacemos trap con EXIT
  exit $s
}
trap 'f_error_trap $BASH_LINENO ${FUNCNAME:-}' ERR

ayuda() {
  f_pinta W "Comandos aceptados:"
  f_pinta H "vigilantecasero.sh [avisa|foto|mensaje|chatinfo] [texto|archivo|nada]"
  f_pinta H "vigilantecasero.sh install"
  f_pinta H "vigilantecasero.sh limpieza"
}

if [[ -z ${1:-} ]]; then
  f_pinta W 'Este script requiere al menos 1 parametro'
  ayuda
  exit 90
fi

v_comando="$1"       # Comando con el que se llama este script desde CLI o motion
v_argumento="${2:-}" # Parametro que acompaña al comando
V_PRESENCIA=false    # Testigo para saber si estamos en casa

# Mandatory variables from $configuracion {/usr/local/etc/}vigilantecasero.conf
# TOKEN=
# IDR=
# RUTAVIDEOS='/media/usb0/motion'

# Default values
IPs2WATCH=""      # IPs que nos sirven para detectar si estamos en casa
TIEMPOMIN=240     # Minutos antes de volver a avisar de eventos si estamos en casa
TIEMPORET=5       # Periodo de gracia en minutos para evaluar si estamos en casa
DIASPARAENCASA=3  # Dias que guardamos los vídeos en los que estabamos en casa
DIASPARANOCASA=14 # Dias que guardamos los vídeos en los que NO estabamos en casa
NUMFOTOEXTRA=0    # Foto adicionalesa mandar en un preaviso
MODO=normal       # normal=todos los avisos. silencioso=sin verbose
DEPURA=false      # Modo depuración

f_get_ruta $etcdir "$configuracion"
# shellcheck source=vigilantecasero.conf
f_get_config "$rutafichero"

# Constants
readonly URL="https://api.telegram.org/bot$TOKEN"
readonly ID="-100$IDR"
readonly PRESENTE='/tmp/vigilantecasero.tmp'
readonly PREAVISO='/tmp/vigilantecasero.preaviso'
readonly FILESNAP="/tmp/vigisnap.jpg"
IPMAQUINA=$(hostname -I | cut -f1 -d" ")

f_mensaje_ss() {
  local texto=${1:-$v_argumento}
  f_pinta H "Mandando mensaje silencioso: $texto"
  curl -s -F "text=$texto" -F "chat_id=$ID" -F disable_notification=true "$URL/sendMessage"
}

f_t_mensaje() {
  local texto=${1:-$v_argumento}
  f_pinta H "Mandando mensaje: $texto"
  MESSAGE_ID=$(curl -s -F "text=$texto" -F "chat_id=$ID" "$URL/sendMessage" | grep -oP '(?<=message_id":)[^,]*')
}

f_t_foto() {
  local foto=${1:-$v_argumento}
  local caption=${2:-}
  f_pinta H "Mandando foto: $foto"
  MESSAGEF_ID=$(curl -s -F "photo=@$foto" -F "caption=$caption" -F "chat_id=$ID" "$URL/sendphoto" | grep -oP '(?<=message_id":)[^,]*')
}

f_t_delete() {
  local message_id=$1
  f_pinta H "Eliminando mensaje: $message_id"
  curl -s -F "message_id=$message_id" -F "chat_id=$ID" "$URL/deleteMessage"
}

f_limpieza() {
  #mkdir -p $RUTAVIDEOS/keep # para que funcione el f_limpieza
  find "$RUTAVIDEOS"/*/ -type f -mmin +$((60 * 24 * DIASPARAENCASA)) -delete
  find "$RUTAVIDEOS" -type f -mmin +$((60 * 24 * DIASPARANOCASA)) -delete
}

f_mandaIP() {
  local alertaIP
  alertaIP=$(f_check_ip) #funcion sacada de dmc_bashelper.sh
  if [[ $alertaIP ]]; then
    f_mensaje_ss "$alertaIP"
  fi
}

f_marca_presencia() {
  V_PRESENCIA=true
  touch "$PRESENTE"
  f_pinta H "$ip detectada en casa con $1"
}

f_estamosencasa() {
  #Hacemos dos pasadas por si la primera no responde el dispositivo
  for ip in $IPs2WATCH $IPs2WATCH; do
    if ping -qc 1 -W 1 "$ip" &>/dev/null; then
      f_marca_presencia PING
      break # Con encontrar un dispositivo ya no seguimos
    elif f_existe_comando arp-scan && arp-scan -x "$ip/32" | grep -q "$ip"; then
      # Hay dispositivos que al entrar en suspensión no responden a ping IP pero si a ARP
      f_marca_presencia ARP
      break # Con encontrar un dispositivo ya no seguimos
    else
      # Solo para DEBUG
      [[ $DEPURA = false ]] || f_pinta W "No detecto la presencia de la fuerza. ($ip)"
    fi
  done
}

f_action_snap() {
  # http://www.lavrsen.dk/foswiki/bin/view/Motion/RemoteControlHttp
  curl -s -o /dev/null http://localhost:8080/0/action/snapshot

} # No se usa

f_take_manualsnap() {
  [[ $DEPURA = false ]] || f_pinta W "f_take_manualsnap : Realizando foto manual"
  # https://www.raspberrypi.org/documentation/usage/camera/raspicam/raspistill.md
  #raspistill -o $FILESNAP # Solo captura de la camara RPi

  f_existe_comando fswebcam && fswebcam -q -d /dev/video2 -r 640x480 --no-banner --jpeg 95 $FILESNAP

  # https://trac.ffmpeg.org/wiki/Capture/Webcam
  #f_existe_comando ffmpeg && ffmpeg -f video4linux2 -video_size 640x480 -input_format yuyv422 -i /dev/video1 -f image2 -frames:v 1 -qscale:v 2 $FILESNAP
}

f_preaviso() {
  local evento=${1:-}
  local msg_preaviso="PREAVISO($evento) de movimiento. $IPMAQUINA:8080"
  f_take_manualsnap || f_pinta W "NO es posible tomar captura manual."
  f_estamosencasa
  if [[ $V_PRESENCIA = "false" ]]; then
    [[ $DEPURA = false ]] || f_pinta W "PREAVISO($evento) de movimiento."
    f_t_mensaje "$msg_preaviso" && echo "$MESSAGE_ID" >$PREAVISO
    f_fichero_usable $FILESNAP && f_t_foto "$FILESNAP" "$msg_preaviso" && echo "$MESSAGEF_ID" >>$PREAVISO
    for _ in $(seq $NUMFOTOEXTRA); do
      f_take_manualsnap && f_t_foto "$FILESNAP" "$msg_preaviso" && echo "$MESSAGEF_ID" >>$PREAVISO
    done
  fi
  f_borraf "$FILESNAP"
}

f_vigila() {
  #f_fichero_usable $PREAVISO && f_t_delete "$(cat $PREAVISO)" && f_borraf "$PREAVISO"
  if f_fichero_usable $PREAVISO; then
    while IFS= read -r line; do
      f_t_delete "$line" # Elimina en Telegram el mensaje con id de esta linea
    done <$PREAVISO
    f_borraf "$PREAVISO" # Una vez eliminandos todos borramos el fichero
  fi
  f_estamosencasa
  if [[ $V_PRESENCIA = "true" ]]; then
    if [[ $(find "$PRESENTE" -cmin +${TIEMPOMIN}) ]] || [[ ! -f $PRESENTE ]]; then
      f_mensaje_ss "$ip detectada en casa"
      algolleno=$(f_check_disk85)
      [[ -z $algolleno ]] || f_mensaje_ss "$algolleno"
    fi
    [[ $DEPURA = false ]] || f_pinta H "Moviendo : ${v_argumento/jpg/mkv} a $RUTAVIDEOS/$ip/"
    #touch "$PRESENTE"
    mkdir -p "$RUTAVIDEOS/$ip"
    mv "${v_argumento/jpg/mkv}" "$RUTAVIDEOS/$ip/" || f_mensaje_ss "ERROR moviendo $v_argumento a $RUTAVIDEOS/$ip/"
  elif
    find "$PRESENTE" -cmin -${TIEMPORET} | grep -q .
  then
    [[ $DEPURA = false ]] || f_pinta W "Detectado movimiento pero habia alguien conocido en casa hace < de ${TIEMPORET}"
    [[ $MODO = "normal" ]] && f_mensaje_ss "Detectado movimiento pero habia alguien conocido en casa hace < de ${TIEMPORET} minutos"
  else
    f_borraf "$PRESENTE"
    f_pinta W "ALARMA: $v_argumento"
    f_t_foto "$v_argumento"
    f_mandaIP
  fi
  f_borraf "$v_argumento"
}

install_v4l2loop() {
  # https://github.com/umlaeute/v4l2loopback
  apt-get install -y raspberrypi-kernel-headers
  apt-get install -y v4l2loopback-dkms # https://github.com/RPi-Distro/repo/issues/188
  echo 'options v4l2loopback video_nr=2' >/etc/modprobe.d/v4l2loopback.conf
  echo v4l2loopback >/etc/modules-load.d/v4l2loopback.conf
  lsmod | grep v4l2loopback || modprobe v4l2loopback || apt reinstall v4l2loopback-dkms

  f_existe_comando fswebcam || apt-get install -y fswebcam
  # https://motion-project.github.io/motion_config.html#OptDetail_Pipe
}

install_arp_scan() {
  f_existe_comando arp-scan || apt-get install -y arp-scan
  chmod u+s /usr/sbin/arp-scan
}

install_motion() {
  f_pinta H "Empezando instalación de motion"
  if stat -f -c %T "$RUTAVIDEOS" | grep "ext[3,4]"; then
    # shellcheck disable=SC2174
    mkdir -m 770 -p "$RUTAVIDEOS"
    chgrp video "$RUTAVIDEOS"
  else
    f_pinta W "Asegurate de tener tu $RUTAVIDEOS montado en /etc/fstab"
    f_pinta W "Y que el usuario motion tiene permisos de escritura en esa carpeta"
    #echo '/dev/sda1 /media/usb0 vfat defaults,noatime,nofail,x-systemd.device-timeout=10s,umask=000 0 2'
  fi
  mkdir -p "$RUTAVIDEOS/keep" # para que funcione el f_limpieza
  f_existe_comando motion || apt-get install -y motion
  local confmotion='/etc/motion/motion.conf'
  f_pinta H "Configurando $confmotion"
  cp $confmotion $confmotion.bak
  #sed -i 's/^daemon off/daemon on/' $confmotion # Con systemd tiene que estar a off
  sed -i 's#^log_file.*#log_file /tmp/motion.log#' $confmotion # Dietpi tiene rw para /var/log solo con root
  sed -i "s#^target_dir.*#target_dir $RUTAVIDEOS#" $confmotion
  sed -i 's#^stream_localhost on.*#stream_localhost off#' $confmotion
  sed -i 's#^webcontrol_localhost on.*#webcontrol_localhost off#' $confmotion
  ## 00 https://motion-project.github.io/motion_config.html#webcontrol_parms
  sed -i 's/^webcontrol_parms.*/webcontrol_parms 1/' $confmotion
  sed -i 's/^threshold .*/threshold 8000/' $confmotion
  sed -i 's/^picture_output.*/picture_output best/' $confmotion || f_pinta W 'Version antigua: output_pictures'
  sed -i 's/^picture_filename.*/picture_filename preview/' $confmotion
  sed -i 's/^movie_filename.*/movie_filename %Y-%m-%d_%H%M%S/' $confmotion
  sed -i 's/^max_movie_time.*/movie_max_time 240/' $confmotion
  sed -i 's/^framerate.*/framerate 2/' $confmotion
  ## 45/60 https://motion-project.github.io/motion_config.html#movie_quality
  sed -i 's/^movie_quality.*/movie_quality 99/' $confmotion || f_pinta W 'Version antigua : ffmpeg_variable_bitrate'
  sed -i "s#.*on_event_start .*#on_event_start $bindir/$SCRIPTNAME preavisa %v >> /tmp/vigilantecasero.log 2>\&1#" $confmotion

  # No estan en la config por defecto
  ## 50 https://motion-project.github.io/motion_config.html#stream_quality
  f_establece_parametros $confmotion stream_quality ' 100'
  ## 400000 https://motion-project.github.io/motion_config.html#movie_bps
  sed -i 's/^movie_bps.*/movie_bps 900000/' $confmotion || f_pinta W 'Version antigua: ffmpeg_bps'
  ## 75 https://motion-project.github.io/motion_config.html#picture_quality
  f_establece_parametros $confmotion picture_quality ' 99' || f_pinta W 'Version antigua : quality'
  ## 00 https://motion-project.github.io/motion_config.html#lightswitch_percent
  f_establece_parametros $confmotion lightswitch_percent ' 10' || f_pinta W 'Version antigua : lightswitch'
  ## 640 https://motion-project.github.io/motion_config.html#width
  sed -i 's/^width.*/width 640/' $confmotion
  ## 640 https://motion-project.github.io/motion_config.html#height
  sed -i 's/^height.*/height 480/' $confmotion
  f_establece_parametros $confmotion on_picture_save " $bindir/$SCRIPTNAME avisa %f >> /tmp/vigilantecasero.log 2>\&1"
  f_establece_parametros $confmotion native_language ' off'

  # DEPRECATED
  #sed -i 's/start_motion_daemon=no/start_motion_daemon=yes/' /etc/default/motion

  f_pinta H "Activamos el puerto CSI para la Cámara"
  f_existe_comando raspi-config && raspi-config nonint do_camera 0 || /boot/dietpi/func/dietpi-set_hardware rpi-camera enable

  install_v4l2loop
  f_establece_parametros $confmotion video_pipe ' /dev/video2'
  install_arp_scan

  f_pinta W "Fin de la instalacion de motion. Es recomendable reiniciar la placa"
}

f_instalar() {
  f_pinta H "Comienza el proceso de instalación del proyecto"
  check_root
  get_script_info
  install_motion
  install -C -v -o root -g root -m 664 "$SCRIPTPATH/$funciones" -t $bindir || f_pinta W "Helper ya instalado? o fichero no encontrado"
  install -C -v -o motion -g root -m 570 "$SCRIPT" -t $bindir || f_pinta W "¿Script ya instalado? o fichero no encontrado"
  install -C -v -o motion -g root -m 460 "$SCRIPTPATH/$configuracion" -t $etcdir || f_pinta W "¿Configuración ya instalado? o fichero no encontrado"
  systemctl enable motion --now
  f_pinta H "Fin de la instalacion del proyecto"
}

case $v_comando in
avisa | a)
  f_vigila
  ;;
preavisa | p)
  f_preaviso "$v_argumento"
  ;;
mensaje | m)
  f_t_mensaje
  ;;
foto | f)
  f_t_foto
  ;;
chatinfo)
  curl -s -X POST "$URL/getChat" -d chat_id="$ID"
  ;;
limpieza | l)
  find "$RUTAVIDEOS" -type f -delete
  ;;
install | i)
  f_instalar
  ;;
*)
  ayuda
  ;;
esac

f_limpieza
f_pinta H "FIN"

# https://core.telegram.org/bots/api
# Como sacar el ID de un canal
# https://stackoverflow.com/questions/33858927/how-to-obtain-the-chat-id-of-a-private-telegram-channel

# Syntaxis alternativa : curl -X POST "$URL/$mandaMesnaje" -d chat_id="$ID" -d text="$MENSAJE"
